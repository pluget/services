import spacy
import sys
import re
from argparse import ArgumentParser

def getLabelsOutOfText(text):
    # Load the trained model
    nlp = spacy.load('./data/models/model-best')

    # Process text with the loaded model
    doc = nlp(text)

    # Iterate over the entities and print their label and text
    labels = []
    for ent in doc.ents:
        if ent.label_ == "PRODUCT":
            labels.append(ent.text)

    return labels

def main():
    parser = ArgumentParser()
    parser.add_argument("-v", "--variant", dest="variant", default=0,
                    help="Set variant number", type=int)
    parser.add_argument("-i", "--id", dest="id", default=-1,
                    help="Set ID number, used when script runs out of options", type=int)

    # Parse arguments
    args = parser.parse_args()
    # Get text input from stdin
    initText = sys.stdin.read()
    
    # Get labels from initText
    labels = list(map(lambda x: re.sub(r"\s+",' ',re.sub(r"[^a-z0-9]", "", x.lower())).strip(), getLabelsOutOfText(initText)))
    # Remove empty strings
    while("" in labels):
        labels.remove("")
    
    if args.variant == 0:
        # Variant 0: Print first label, if does not exist, print ID
        if len(labels) < 1:
            print(args.id)
        else:
            print(labels[0])
    else:
        # Remove duplicates
        labels = list(dict.fromkeys(labels))
        # Variant n: Print nth label, if does not exist, print first label with ID
        if len(labels) < args.variant:
            print(args.id)
        else:
            print(labels[args.variant-1])


if __name__ == '__main__':
    main()
